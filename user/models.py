from django.db import models
from django.contrib.auth.models import User
from ckeditor_uploader.fields import RichTextUploadingField


# Profile model for users
class Profile(models.Model):
    TYPE_CHOICES = (
        ("t", "Teacher"),
        ("s", "Student"),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, db_column="User", blank=True, null=True)
    fullname = models.CharField(db_column="Name", max_length=250, blank=True, null=True)
    avatar = models.CharField(db_column="Avatar", default="/static/home/images/author.jpg", max_length=250, blank=True, null=True)
    date_of_birth = models.DateField(db_column="Birth", blank=True, null=True)
    address = models.CharField(db_column="Address", max_length=30, blank=True, null=True)
    create_date = models.DateField(db_column="CreateDate", auto_now_add=True, blank=True, null=True)
    phone = models.CharField(db_column="Phone", max_length=30, blank=True, null=True)
    email = models.EmailField(db_column="Email", max_length=250, blank=True, null=True)
    bio = RichTextUploadingField(db_column="Bio", blank=True, null=True)
    account_type = models.CharField(db_column="Type", choices=TYPE_CHOICES, max_length=100, null=True)
    is_active = models.BooleanField(db_column="Active", default=False, blank=True, null=True)
    is_enable = models.BooleanField(db_column="Enable", default=False, blank=True, null=True)

    class Meta:
        managed = True
        db_table = "Profile"