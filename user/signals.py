from django.dispatch import Signal


user_signup_signal = Signal(providing_args=["user"])