from .models import *
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime

@csrf_exempt
def check_username(request):
    username = request.POST.get('username', None)
    print(username)

    if username == '':
        data = {
            'flag': 2,
        }
    else:
        is_taken = User.objects.filter(username=username)
        if is_taken:
            data = {
                'flag': 1,
            }
        else:
            data = {
                'flag': 0,
            }
    
    return JsonResponse(data)

@csrf_exempt
def check_email(request):
    email = request.POST.get('email', None)
    print(email)

    if email == '':
        data = {
            'flag': 2,
        }
    else:
        is_taken = User.objects.filter(email=email)
        # đã tồn tại email
        if is_taken:
            data = {
                'flag': 1,
            }
        # chưa tồn tại email
        else:
            # check đuôi email theo dạng @gmail.com hoặc @student.hcmue.edu.vn
            tail_of_email = email.split('@')[1]
            if (tail_of_email == 'gmail.com' or tail_of_email == 'student.hcmue.edu.vn'):
                data = {
                    'flag': 0,
                }
            else:
                data = {
                    'flag': 2,
                }
    
    return JsonResponse(data)

@csrf_exempt
def check_dob(request):
    dob = request.POST.get('dob', None)
    print(dob)  #yyyy-mm-dd

    if dob == '':
        data = {
            'flag': 2,
        }
    else:
        # split day, month, year
        dob = dob.split('-')
        year = int(dob[0])
        month = int(dob[1])
        day = int(dob[2])

        # get current year
        current_year = datetime.now().year

        # check đủ 18 tuổi trở lên
        if current_year - year >= 18:
            # check ngày hợp lệ theo tháng
            month_has_31 = [1, 3, 5, 7, 8, 10, 12]
            # check nếu là ngày 31 -> check ngày có nằm trong đúng tháng hay không
            # check nếu là ngày 30/2
            # check nếu là ngày 29/2 -> check năm nhuận hay không nhuận
            if (day == 31 and (month not in month_has_31)) or ((day == 30) and (month == 2)) or ((day == 29) and (month == 2) and not ((year % 400 == 0) or ((year % 4 == 0) and (year % 100 != 0)))):
                data = {
                    'flag': 2
                }
            # xác nhận hợp lệ
            else:
                data = {
                    'flag': 0
                }
        # chưa đủ 18 tuổi -> đưa ra cảnh báo
        else:
            data = {
                'flag': 1
            }
        
    return JsonResponse(data)