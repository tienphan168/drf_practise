from django.urls import path
from user import views, ajax

app_name = 'user'

urlpatterns = [
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path("logout/", views.logout, name="logout"),
    path("register/check/username/", ajax.check_username, name="check_username"),
    path("register/check/email/", ajax.check_email, name="check_email"),
    path("register/check/dob/", ajax.check_dob, name="check_dob"),

]