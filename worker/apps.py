from django.apps import AppConfig
from django.utils.module_loading import autodiscover_modules


class WorkerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'worker'

    def ready(self):
        autodiscover_modules('tasks') 
        from . import signals 
        from .tasks import send_confirmation_email  
        signals.user_signed_up.connect(send_confirmation_email)