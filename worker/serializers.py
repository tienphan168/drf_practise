from rest_framework import serializers
from .tasks import add_category
from django.contrib.auth.models import User
from .signals import user_signed_up


class CreateCategory(serializers.Serializer):
    def create(self, validated_data):
        category_name = validated_data.get('name')
        category_id = add_category.delay(category_name).get()
        return category_id
    

class UserWorkerSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "username")
        read_only_fields = fields

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)

        # Trigger the user_signed_up signal
        user_signed_up.send(sender=self.__class__, user_email=[user[i].email for i in user])