from celery import shared_task
from catalog.models import Category
from django.core.mail import send_mail
from home.celery import app


@app.task
def send_confirmation_email(sender, user_email, **kwargs):
    # Send the confirmation email
    subject = 'Account Confirmation'
    message = 'Thank you for signing up! Your account has been successfully registered.'
    from_email = 'tienpm.ttth@gmail.com'
    recipient_list = [user_email]
    send_mail(subject, message, from_email, recipient_list)


@app.task
def add_category(codename, category_name):
    category = Category(name=category_name, codename=codename)
    category.save()
    return category.id