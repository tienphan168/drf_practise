from django.contrib import admin
from django.urls import path, include
from django.views.static import serve
from django.urls import re_path as url
from django.conf import settings
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('catalog.urls')),
    path('', include('user.urls')),
    path('', include('home.urls')),
    path("accounts/", include("allauth.urls")),
    path('api-schema/', SpectacularAPIView.as_view(), name='schema'),
    path('api-schema/docs/', SpectacularSwaggerView.as_view(url_name='schema')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify/', TokenVerifyView.as_view(), name='token_verify'),


    # set up an API with ckeditor
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
]

urlpatterns += url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
urlpatterns += url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
