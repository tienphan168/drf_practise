from rest_framework import serializers
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'username', 'password']
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance
    

# class ReportUserSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = ReportUser
#         fields = "__all__"
#         read_only_fields = ("from_user", "archived")

#     def create(self, validated_data):
#         from_user = self.context["request"].user
#         validated_data.update(from_user=from_user)
#         instance = super().create(validated_data)
#         to_user = instance.to_user
#         count = ReportUser.objects.filter(to_user=to_user, archived=False).distinct("from_user").count()
#         if count >= settings.NUMBER_USER_REPORT:
#             to_user.is_banned = True
#             to_user.save()
#         return instance
