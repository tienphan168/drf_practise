// Variables

var passwordElement = document.getElementById("password1");
var passwordElement2 = document.getElementById("password2");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var minlength = document.getElementById("minlength");
var special = document.getElementById("special");
var checkpass2 = document.getElementById("checkpass2");

// Functions

// Check username, email
$(document).ready(function() {
    $("#username").change(function() {
        var username = $(this).val();

        $.ajax({
            url: 'check/username/',
            data: {
                'username': username,
            },
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                console.log(data.flag)
                if (data.flag == 0) {
                    document.getElementById("icon-success1").style.display = 'flex';
                    document.getElementById("icon-fail1").style.display = 'none';
                    document.getElementById("checkusername").innerHTML = '';
                }
                else if (data.flag == 1) {
                    document.getElementById("icon-success1").style.display = 'none';
                    document.getElementById("icon-fail1").style.display = 'flex';
                    document.getElementById("checkusername").innerHTML = 'Tên đăng nhập đã tồn tại.';
                } 
                else {
                    document.getElementById("icon-success1").style.display = 'none';
                    document.getElementById("icon-fail1").style.display = 'flex';
                    document.getElementById("checkusername").innerHTML = 'Tên đăng nhập không được để trống.';
                }

            },
        })
    })

    $("#email").change(function() {
        var email = $(this).val();
        // kiểm tra email có đúng đuôi dạng name@string.string
        if ((email != '') && (email.match(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/
            ))) 
        {
            $.ajax({
                url: 'check/email/',
                data: {
                    'email': email,
                },
                type: 'POST',
                dataType: 'json',
                success: function(data) {
                    console.log(data.flag)
                    if (data.flag == 0) {
                        document.getElementById("icon-success2").style.display = 'flex';
                        document.getElementById("icon-fail2").style.display = 'none';
                        document.getElementById("checkemail").innerHTML = '';
                    }
                    else if (data.flag == 1) {
                        document.getElementById("icon-success2").style.display = 'none';
                        document.getElementById("icon-fail2").style.display = 'flex';
                        document.getElementById("checkemail").innerHTML = 'Email này đã được sử dụng.';
                    } 
                    else if (data.flag == 2) {
                        document.getElementById("icon-success2").style.display = 'none';
                        document.getElementById("icon-fail2").style.display = 'flex';
                        document.getElementById("checkemail").innerHTML = 'Email buộc phải có đuôi @gmail.com hoặc @student.hcmue.edu.vn';
                    }
                    // else {
                    //     document.getElementById("icon-success2").style.display = 'none';
                    //     document.getElementById("icon-fail2").style.display = 'flex';
                    //     document.getElementById("checkemail").innerHTML = 'Email không được để trống.';
                    // }

                },
            })
        }
        else {
            document.getElementById("icon-success2").style.display = 'none';
            document.getElementById("icon-fail2").style.display = 'flex';
            document.getElementById("checkemail").innerHTML = 'Email chưa đúng định dạng.';
        }
    })

    // check birth of date
    $("#dob").change(function() {
        var dob = $(this).val();

        $.ajax({
            url: 'check/dob/',
            data: {
                'dob': dob,
            },
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                console.log(data.flag)
                if (data.flag == 0) {
                    document.getElementById("icon-success4").style.display = 'flex';
                    document.getElementById("icon-fail4").style.display = 'none';
                    document.getElementById("checkdob").innerHTML = '';
                }
                else if (data.flag == 1) {
                    document.getElementById("icon-success4").style.display = 'none';
                    document.getElementById("icon-fail4").style.display = 'flex';
                    document.getElementById("checkdob").innerHTML = 'Yêu cầu từ 18 tuổi trở lên.';
                } 
                else if (data.flag == 2) {
                    document.getElementById("icon-success4").style.display = 'none';
                    document.getElementById("icon-fail4").style.display = 'flex';
                    document.getElementById("checkdob").innerHTML = 'Ngày sinh không hợp lệ.';
                }

            },
        })
    })
    
    // check checkbox of term-conditions
    $('input[type="checkbox"]').click(function(){
        if($(flexCheckDefault4).prop("checked") == true){
            $("#register").css("opacity", 1);
            $("#register").prop( "disabled", false )
        }
        else if($(flexCheckDefault4).prop("checked") == false){
            // làm mờ và vô hiệu hóa button đăng ký
            document.getElementById('register').style.setProperty('opacity', '0.5', 'important')
            $("#register").prop( "disabled", true )
        }
    });
})

// Check password
passwordElement.onfocus = function() {
    document.getElementById("message").style.display = "block";
    checkpass2.style.display = 'none';
}

passwordElement.onblur = function() {
    document.getElementById("message").style.display = "none";
    checkpass2.style.display = 'none';
}

passwordElement2.onfocus = function() {
    document.getElementById("message").style.display = "block";
    checkpass2.style.display = 'block';
}

passwordElement2.onblur = function() {
    document.getElementById("message").style.display = "none";
    checkpass2.style.display = 'none';
}

passwordElement.onkeyup = function() {
    // Set flag default
    let flag = true;

    // Validate lowercase letters
    let lowerCaseLetters = /[a-z]/g;
    if (passwordElement.value.match(lowerCaseLetters)) {
        letter.style.display = 'none';
    }
    else {
        letter.style.display = 'block';
        flag = false;
    }

    // Validate uppercase letters
    let upperCaseLetters = /[A-Z]/g;
    if (passwordElement.value.match(upperCaseLetters)) {
        capital.style.display = 'none';
    }
    else {
        capital.style.display = 'block';
        flag = false;
    }

    // Validate numbers
    let numbers = /[0-9]/g;
    if (passwordElement.value.match(numbers)) {
        number.style.display = 'none';
    }
    else {
        number.style.display = 'block';
        flag = false;
    }

    // Validate special char
    let specials = /[!@#$%^&*_=+-]/g;
    if (passwordElement.value.match(specials)) {
        special.style.display = 'none';
    }
    else {
        special.style.display = 'block';
        flag = false;
    }

    // Validate length
    if (passwordElement.value.length >= 8) {
        minlength.style.display = 'none';
    }
    else {
        minlength.style.display = 'block';
        flag = false;
    }

    // Check flag
    if (flag == true) {
        document.getElementById("icon-success3").style.display = 'flex';
        document.getElementById("icon-fail3").style.display = 'none';
    }
    else {
        document.getElementById("icon-success3").style.display = 'none';
        document.getElementById("icon-fail3").style.display = 'flex';
    }
}

passwordElement2.onkeyup = function() {
    // Equal password
    if (passwordElement.value == passwordElement2.value) {
        checkpass2.style.display = 'none';
    }
    else {
        checkpass2.style.display = 'block';
        flag = false;
    }
}
/* END CHECK FOR REGISTER */ 