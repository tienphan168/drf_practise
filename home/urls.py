from django.urls import path
from home import views
from rest_framework.authtoken.views import obtain_auth_token

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('login-api/', obtain_auth_token),
    path('api/register/', views.RegisterView.as_view()),
    path('api/login/', views.LoginView.as_view()),
    path('api/user/', views.UserView.as_view()),
    path('api/logout/', views.LogoutView.as_view()),
]