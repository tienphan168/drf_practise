from django.shortcuts import render
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile

from .models import Category, Product, Image
from .serializers import (
    CategorySerializer, ProductSerializer, ImageSerializer, ReadImageSerializer, WriteImageSerializer,
    ReportEntrySerializer, ReportParamsSerializer
)

from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from django_filters.rest_framework import DjangoFilterBackend
from .reports import product_report


def products(request):
    products = Product.objects.all()   

    context = {
        'products': products
    }
    return render(request, 'home/products.html', context)

def product_change(request, id):
    categories = Category.objects.all().filter(is_enable=True)

    if id == 0 or id == '0':
        if request.method == 'POST':
            title = request.POST.get('title')
            description = request.POST.get('description')
            images = request.FILES.getlist('photo')
            categories = request.POST.getlist('category')
            print(categories)
            list_image = []
            for index, image in enumerate(images):
                path_image = default_storage.save(
                    "product/photo/" + str(product.slug) + str(index) + ".png", ContentFile(image.read())
                )
                list_image.append(Image(link="/media/" + path_image, product=product))
            if list_image:
                Image.objects.bulk_create(list_image)

            product = Product.objects.create(title=title, description=description)
        return render(request, 'home/product_change.html', context)
    else:
        product = Product.objects.get(id=id)
        if request.method == 'POST':
            product.title = request.POST.get('title') or product.title
            product.description = request.POST.get('description') or product.description
            images = request.FILES.getlist('photo')
            categories = request.POST.getlist('category')
            print(categories)
            list_image = []
            for index, image in enumerate(images):
                path_image = default_storage.save(
                    "product/photo/" + str(product.slug) + str(index) + ".png", ContentFile(image.read())
                )
                list_image.append(Image(link="/media/" + path_image, product=product))
            if list_image:
                Image.objects.bulk_create(list_image)
            
            product.save()

        context = {
            'product': product,
            'categories': categories,
        }
        return render(request, 'home/product_change.html', context)

######################################################################

class CategoryListAPIView(ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    pagination_class = None


class CategoryModelViewSet(ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ProductModelViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [SearchFilter, OrderingFilter, DjangoFilterBackend]
    search_fields = ("link",)
    ordering_fields = ['create_date', 'id']
    filterset_fields = ['title',]

    def get_queryset(self):
        return Product.objects.select_related("user").filter(user=self.request.user)
    
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    # def get_serializer_class(self):
    #     if self.action in ("list", "retrieve"):
    #         return ReadProductSerializer
    #     return WriteProductSerializer


class ImageModelViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Image.objects.select_related("product")
    serializer_class = ImageSerializer
    filter_backends = [SearchFilter, OrderingFilter, DjangoFilterBackend]
    search_fields = ("link",)
    ordering_fields = ['create_date', 'id']
    filterset_fields = ['product__title',]

    def get_serializer_class(self):
        if self.action in ("list", "retrieve"):
            return ReadImageSerializer
        return WriteImageSerializer
    

class ProductReportAPIView(APIView):
    def get(self, request):
        params_serializer = ReportParamsSerializer(data=request.GET, context={"request": request})
        params_serializer.is_valid(raise_exception=True)
        params = params_serializer.save()
        data = product_report(params)
        serializer = ReportEntrySerializer(instance=data, many=True)
        return Response(data=serializer.data)