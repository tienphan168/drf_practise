from django.db import models
from django.contrib.auth.models import User
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.text import slugify

# Create your models here.
class Category(models.Model):
    id = models.AutoField(db_column="Category_ID", primary_key=True)
    codename = models.CharField(db_column="Category_codename", max_length=250, unique=True)
    name = models.CharField(db_column="Category_name", max_length=250, blank=True, null=True)
    image = models.CharField(max_length=250, blank=True, null=True)
    note = models.CharField(db_column="Note", max_length=250, blank=True, null=True)
    create_date = models.DateField(db_column="Created_date", auto_now_add=True, blank=True, null=True)
    is_enable = models.BooleanField(db_column="Enable", default=True, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = True
        db_table = "Category"

class Product(models.Model):
    id = models.AutoField(primary_key = True)
    title = models.CharField(max_length=250, blank=True, null=True)
    category = models.ManyToManyField(Category, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='product', null=True)
    description = RichTextUploadingField(blank=True, null=True)
    content = RichTextUploadingField(blank=True, null=True)
    type = models.CharField(max_length=250, blank=True, null=True)
    create_date = models.DateField(db_column="Created_date", auto_now_add=True, blank=True, null=True)
    edit_date = models.DateTimeField(auto_now=True, blank=True, null=True) 
    is_enable = models.BooleanField(default=True, blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    slug = models.SlugField(unique=True, max_length=250, null=True, blank=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        slug = originalslug = slugify(self.title)
        i=0
        while Product.objects.filter(slug=slug).exists():
            temp = Product.objects.filter(slug=slug)
            if temp[0].slug == self.slug:
                break
            else:
                slug = '{}-{}'.format(originalslug, i)
                i += 1
        self.slug = slug
        super(Product, self).save(*args, **kwargs)
        
    class Meta:
        managed = True
        db_table = 'Product'

class Image(models.Model):
    id = models.AutoField(primary_key=True)
    link = models.CharField(db_column="Link", max_length=300, null=True, blank=True)
    product = models.ForeignKey("Product", on_delete=models.CASCADE, null=True, blank=True)
    is_enable = models.BooleanField(default=True, blank=True, null=True)
    create_date = models.DateField(db_column="Created_date", auto_now_add=True, blank=True, null=True)
    edit_date = models.DateTimeField(auto_now=True, blank=True, null=True) 

    class Meta:
        managed = True
        db_table = 'Image'


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    content = models.TextField()
    create_date = models.DateField(auto_now_add=True, blank=True, null=True)
    edit_date = models.DateTimeField(auto_now=True, blank=True, null=True) 
    product = models.ForeignKey('Product', on_delete=models.CASCADE)
    is_enable = models.BooleanField(default=True, blank=True, null=True)
    note = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.body[:20]

    class Meta:
        managed = True
        db_table = 'Comment'