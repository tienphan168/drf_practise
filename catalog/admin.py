from django.contrib import admin
from .models import *


class CategoryInline(admin.TabularInline):
    model = Product.category.through

# Register your models here.
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("codename", "name", "is_enable")
    list_filter = ("create_date",)

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ("title", "is_enable", "list_category")

    def list_category(self, obj):
        categories = obj.category.all()
        list_category = []
        for category in categories:
            list_category.append(category.codename)
        return list_category

@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ("link", "product", "is_enable")