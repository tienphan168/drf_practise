from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Category, Product, Image
from .reports import ReportParams


class ReadUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "username")
        read_only_fields = fields


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ("id", "codename", "name")


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ("id", "title", "description")


class ImageSerializer(serializers.ModelSerializer):
    product = serializers.SlugRelatedField(slug_field="title", queryset=Product.objects.all())

    class Meta:
        model = Image
        fields = ("id", "link", "product")


class WriteImageSerializer(serializers.ModelSerializer):
    product = serializers.SlugRelatedField(slug_field="title", queryset=Product.objects.all())

    class Meta:
        model = Image
        fields = ("id", "link", "product")


class ReadImageSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = Image
        fields = ("id", "link", "product")
        read_only_fields = fields


class ReadProductSerializer(serializers.ModelSerializer):
    user = ReadUserSerializer()

    class Meta:
        model = Category
        fields = (
            "codename",
            "name",
            "image",
            "note",
            "create_date",
            "is_enable",
            "user",
        )
        read_only_fields = fields


class WriteProductSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Product
        fields = (
            "user",
            "title",
            "category",
        )


class ReportEntrySerializer(serializers.Serializer):
    category = CategorySerializer()
    total = serializers.DecimalField(max_digits=15, decimal_places=2)
    count = serializers.IntegerField()
    avg = serializers.DecimalField(max_digits=15, decimal_places=2)


class ReportParamsSerializer(serializers.Serializer):
    start_date = serializers.DateTimeField()
    end_date = serializers.DateTimeField()
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    def create(self, validated_data):
        return ReportParams(**validated_data)