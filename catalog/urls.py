from django.urls import path
from catalog import views

from rest_framework import routers


router = routers.SimpleRouter()

router.register(r'categories', views.CategoryModelViewSet, basename='category')
router.register(r'product', views.ProductModelViewSet, basename='product')
router.register(r'image', views.ImageModelViewSet, basename='image')

app_name = 'catalog'

urlpatterns = [
    path('products/', views.products, name='products'),
    path('products/<int:id>', views.product_change, name='product_change'),
    path("report/", views.ProductReportAPIView.as_view(), name="report"),
    path('category/', views.CategoryListAPIView.as_view(), name='category'),

] + router.urls